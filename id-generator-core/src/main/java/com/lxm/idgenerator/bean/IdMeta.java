package com.lxm.idgenerator.bean;

import com.lxm.idgenerator.enums.IdType;
import com.lxm.idgenerator.util.BitUtil;

/**
 * Id对象元数据, 描述Id的数据结构
 * Id统一8字节(64位)
 * @author luoxiaomin
 * @version 1.0.0
 * @date 2018/6/25
 * @time 11:27
 */
public abstract class IdMeta {

    private byte fixedBits = 1;

    /**
     * 时间戳bit位
     */
    protected byte timeBits = 0;

    /**
     * 机器码bit位
     */
    protected byte workerBits = 0;

    /**
     * 序列bit位
     */
    protected byte seqBits = 0;

    protected IdType type;


    public IdMeta(byte timeBits, byte workerBits, byte seqBits) {
        assert timeBits + workerBits + seqBits != 63 : "时间戳+机器码+序列bit位之和必须等于63位";
        this.timeBits = timeBits;
        this.workerBits = workerBits;
        this.seqBits = seqBits;
    }

    public long getFixedBitsMask() {
        return BitUtil.mask(fixedBits);
    }

    /**
     * 返回时间戳掩码
     * @return
     */
    public long getTimeBitsMask() {
        return BitUtil.mask(timeBits);
    }

    /**
     * 返回机器码掩码
     * @return
     */
    public long getWorkerBitsMask() {
        return BitUtil.mask(workerBits);
    }

    /**
     * 返回序列号掩码
     * @return
     */
    public long getSeqBitsMask() {
        return BitUtil.mask(seqBits);
    }

    /**
     * 返回时间戳起始位置
     * @return
     */
    public long getTimeBitsIndex() {
        return fixedBits;
    }

    /**
     * 返回机器码的起始位置
     * @return
     */
    public long getWorkerBitsIndex() {
        return fixedBits + timeBits;
    }

    /**
     * 返回序列码的启示位置
     * @return
     */
    public long getSeqBitsIndex() {
        return fixedBits + timeBits + workerBits;
    }

    public byte getTimeBits() {
        return timeBits;
    }

    public byte getWorkerBits() {
        return workerBits;
    }

    public IdType getType() {
        return type;
    }

}
